Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SqlParser
Source: https://github.com/phpmyadmin/sql-parser

Files: *
Copyright: 2015-2023 The phpMyAdmin Team <developers@phpmyadmin.net>
License: GPL-2.0+

Files: debian/*
Copyright: 2018-2020 Felipe Sateler <fsateler@debian.org>
           2020-2023 William Desportes <williamdes@wdes.fr>
License: GPL-2.0+

Files:
 locale/af/LC_MESSAGES/sqlparser.po
 locale/ar/LC_MESSAGES/sqlparser.po
 locale/ast/LC_MESSAGES/sqlparser.po
 locale/az/LC_MESSAGES/sqlparser.po
 locale/be@latin/LC_MESSAGES/sqlparser.po
 locale/be/LC_MESSAGES/sqlparser.po
 locale/bg/LC_MESSAGES/sqlparser.po
 locale/bn/LC_MESSAGES/sqlparser.po
 locale/br/LC_MESSAGES/sqlparser.po
 locale/brx/LC_MESSAGES/sqlparser.po
 locale/bs/LC_MESSAGES/sqlparser.po
 locale/ca/LC_MESSAGES/sqlparser.po
 locale/ckb/LC_MESSAGES/sqlparser.po
 locale/cs/LC_MESSAGES/sqlparser.po
 locale/cy/LC_MESSAGES/sqlparser.po
 locale/da/LC_MESSAGES/sqlparser.po
 locale/de/LC_MESSAGES/sqlparser.po
 locale/el/LC_MESSAGES/sqlparser.po
 locale/en_GB/LC_MESSAGES/sqlparser.po
 locale/eo/LC_MESSAGES/sqlparser.po
 locale/es/LC_MESSAGES/sqlparser.po
 locale/et/LC_MESSAGES/sqlparser.po
 locale/eu/LC_MESSAGES/sqlparser.po
 locale/fa/LC_MESSAGES/sqlparser.po
 locale/fi/LC_MESSAGES/sqlparser.po
 locale/fr/LC_MESSAGES/sqlparser.po
 locale/fy/LC_MESSAGES/sqlparser.po
 locale/gl/LC_MESSAGES/sqlparser.po
 locale/gu/LC_MESSAGES/sqlparser.po
 locale/he/LC_MESSAGES/sqlparser.po
 locale/hi/LC_MESSAGES/sqlparser.po
 locale/hr/LC_MESSAGES/sqlparser.po
 locale/hu/LC_MESSAGES/sqlparser.po
 locale/hy/LC_MESSAGES/sqlparser.po
 locale/ia/LC_MESSAGES/sqlparser.po
 locale/id/LC_MESSAGES/sqlparser.po
 locale/it/LC_MESSAGES/sqlparser.po
 locale/ja/LC_MESSAGES/sqlparser.po
 locale/ka/LC_MESSAGES/sqlparser.po
 locale/kk/LC_MESSAGES/sqlparser.po
 locale/km/LC_MESSAGES/sqlparser.po
 locale/kmr/LC_MESSAGES/sqlparser.po
 locale/kn/LC_MESSAGES/sqlparser.po
 locale/ko/LC_MESSAGES/sqlparser.po
 locale/ksh/LC_MESSAGES/sqlparser.po
 locale/ky/LC_MESSAGES/sqlparser.po
 locale/li/LC_MESSAGES/sqlparser.po
 locale/lt/LC_MESSAGES/sqlparser.po
 locale/lv/LC_MESSAGES/sqlparser.po
 locale/mk/LC_MESSAGES/sqlparser.po
 locale/ml/LC_MESSAGES/sqlparser.po
 locale/mn/LC_MESSAGES/sqlparser.po
 locale/ms/LC_MESSAGES/sqlparser.po
 locale/nb/LC_MESSAGES/sqlparser.po
 locale/ne/LC_MESSAGES/sqlparser.po
 locale/nl/LC_MESSAGES/sqlparser.po
 locale/pa/LC_MESSAGES/sqlparser.po
 locale/pl/LC_MESSAGES/sqlparser.po
 locale/pt_BR/LC_MESSAGES/sqlparser.po
 locale/pt/LC_MESSAGES/sqlparser.po
 locale/rcf/LC_MESSAGES/sqlparser.po
 locale/ro/LC_MESSAGES/sqlparser.po
 locale/ru/LC_MESSAGES/sqlparser.po
 locale/si/LC_MESSAGES/sqlparser.po
 locale/sk/LC_MESSAGES/sqlparser.po
 locale/sl/LC_MESSAGES/sqlparser.po
 locale/sq/LC_MESSAGES/sqlparser.po
 locale/sr@latin/LC_MESSAGES/sqlparser.po
 locale/sr/LC_MESSAGES/sqlparser.po
 locale/sv/LC_MESSAGES/sqlparser.po
 locale/ta/LC_MESSAGES/sqlparser.po
 locale/te/LC_MESSAGES/sqlparser.po
 locale/th/LC_MESSAGES/sqlparser.po
 locale/tk/LC_MESSAGES/sqlparser.po
 locale/tr/LC_MESSAGES/sqlparser.po
 locale/tt/LC_MESSAGES/sqlparser.po
 locale/ug/LC_MESSAGES/sqlparser.po
 locale/uk/LC_MESSAGES/sqlparser.po
 locale/ur/LC_MESSAGES/sqlparser.po
 locale/uz@latin/LC_MESSAGES/sqlparser.po
 locale/uz/LC_MESSAGES/sqlparser.po
 locale/vi/LC_MESSAGES/sqlparser.po
 locale/vls/LC_MESSAGES/sqlparser.po
 locale/zh_CN/LC_MESSAGES/sqlparser.po
 locale/zh_TW/LC_MESSAGES/sqlparser.po
Copyright: 2015-2023 The phpMyAdmin Team <developers@phpmyadmin.net>
License: GPL-2.0
Comment: These files are licenced under the same license as the phpMyAdmin package,
 which is GPL-2.0 (no later version)

License: GPL-2.0
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-2'
 file.

License: GPL-2.0+
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-2'
 file.
